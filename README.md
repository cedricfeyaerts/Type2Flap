# Type 2 Flap

Our submission for the game jam [#LDjam41](https://ldjam.com/events/ludum-dare/41)

The theme was **Combine 2 Incompatible Genres**. Not sure we followed all the rules but there is it is. **Type 2 Flap** a cross genre between flapy bird and a typing game.


## where to play

https://cedricfey.itch.io/type2flap

## How to play

Literally type "flap" to take the bird into the air. You can also **"spit"**, **"dash"** and **"dive"**.
You can also try **"Flaaap"** and **"Daaash"** and combine of course.

+ **"Flaap"** flap 2 times
+ **"Flaaap"** flap 3 times and so on. It gives you the time to "spit"

Repeat the last letter of a command to repeat the action:

**"flap" "p" "p"** etc...

P.S It is very hard. 

P.P.S Not kidding it's way too hard