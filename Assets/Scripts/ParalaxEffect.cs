﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxEffect : MonoBehaviour
{
  public float speed;
  private Transform t;
  private float offset;
  private PlayerController controller;
  private GameObject player;

  // Use this for initialization
  void Start()
  {
    t = GetComponent<Transform>();
    player = GameObject.Find("Player");
    controller = player.GetComponent<PlayerController>();
    offset = t.position.x - GetPositionCamera();
  }

  // Update is called once per frame
  void Update()
  {
    if (!controller.playing)
    {
      return;
    }
    offset -= speed;
    t.position = new Vector3(GetPositionCamera() + offset, t.position.y);
  }

  float GetPositionCamera()
  {
    return player.transform.position.x + 5;
  }

}
