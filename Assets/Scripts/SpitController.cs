﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitController : MonoBehaviour
{

  private Rigidbody2D rb2d;
  private Transform t;
  private ScoreController score;

  // Use this for initialization
  void Start()
  {
    t = GetComponent<Transform>();
    rb2d = GetComponent<Rigidbody2D>();
    score = GameObject.Find("Score").GetComponent<ScoreController>();
  }

  // Update is called once per frame
  void Update()
  {
    float zAngle = 45 * Mathf.Atan2(rb2d.velocity.y, rb2d.velocity.x);
    t.eulerAngles = new Vector3(0, 0, zAngle);
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.CompareTag("Enemy"))
    {
      print("kill it");
      Destroy(collision.gameObject);
      score.Increment();
    }
    print("something");
    Destroy(gameObject);
  }
}
