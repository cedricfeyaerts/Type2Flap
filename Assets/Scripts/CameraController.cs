﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

  public GameObject player;

  private Transform t;
  private PlayerController controller;

  // Use this for initialization
  void Start()
  {
    t = GetComponent<Transform>();
    controller = player.GetComponent<PlayerController>();
  }

  // Update is called once per frame
  void Playng()
  {

  }

  // LateUpdate is called once per frame
  void LateUpdate()
  {
    if (!controller.playing)
    {
      return;
    }
    t.position = new Vector3(player.transform.position.x + 5, 0, -10);
  }

}
