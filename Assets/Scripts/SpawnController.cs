﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
  public Transform pipe;
  public GameObject player;

  private float lastSpawn;
  private float spacing;
  private float ahead;
  private float pipeRatio;

  // Use this for initialization
  void Start()
  {
    lastSpawn = 0;
    spacing = 5;
    ahead = 15;
    pipeRatio = 0.5f;
  }

  // Update is called once per frame
  void Update()
  {
    if (player.transform.position.x - lastSpawn > spacing)
    {
      Spawn();
      lastSpawn = player.transform.position.x;
    }
  }

  void Spawn()
  {
    if (Random.Range(0.0f, 1.0f) > pipeRatio)
    {
      Instantiate(pipe, new Vector3(player.transform.position.x + ahead, 0, 0), Quaternion.identity);
    }
  }
}
