﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public GameObject spit;
  public float spitSpeed;
  public float bumpFactor;
  public float baseSpeed;
  private Rigidbody2D rb2d;
  private Transform t;
  private int flapQueue;
  private float lastFlapTime;
  private int spitQueue;
  private float lastSpitTime;
  private int poopQueue;
  private float lastPoopTime;
  private float dashTime;

  private Animator animator;
  public bool playing;

  // Use this for initialization
  void Start()
  {
    Time.timeScale = 0;
    playing = false;
    rb2d = GetComponent<Rigidbody2D>();
    t = GetComponent<Transform>();
    animator = GetComponent<Animator>();

    flapQueue = 0;
    lastFlapTime = 0;
    spitQueue = 0;
    lastSpitTime = 0;
    poopQueue = 0;
    lastPoopTime = 0;
    dashTime = 0;
  }

  // FixedUpdate is called once per fixed frame
  void FixedUpdate()
  {

  }

  // Update is called once per frame
  void Update()
  {
    if (!playing)
    {
      return;
    }

    if (rb2d.position.y < -4f || rb2d.position.y > 6.5f)
    {
      Die();
    }

    if (flapQueue > 0 && Time.time - lastFlapTime > 2.2f)
    {
      flapQueue--;
      Flap();
    }

    Coast();

    float zAngle = 45 * Mathf.Atan2(rb2d.velocity.y, rb2d.velocity.x);
    t.eulerAngles = new Vector3(0, 0, zAngle);
  }

  private void StartPlaying()
  {
    if (!playing)
    {
      Time.timeScale = 1;
      playing = true;
      rb2d.velocity = new Vector2(baseSpeed, 0);
    }
  }

  public void Coast()
  {
    var speed = baseSpeed + Mathf.Min(Mathf.Exp(10 * dashTime) * baseSpeed, 400);
    if (dashTime > 0)
    {
      dashTime -= Time.deltaTime;
    }
    if (dashTime < 0)
    {
      dashTime = 0;
    }
    rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
  }

  public void Flap()
  {
    StartPlaying();
    rb2d.velocity = new Vector2(rb2d.velocity.x, bumpFactor);
    lastFlapTime = Time.time;
    animator.SetTrigger("Flap");
  }

  public void Flap(int repetitions)
  {
    flapQueue += repetitions - 1;
    Flap();
  }
  public void ClearFlaps()
  {
    flapQueue = 0;
  }

  public void Dive()
  {
    rb2d.velocity = new Vector2(rb2d.velocity.x, -bumpFactor);
  }

  public void Spit()
  {
    GameObject currentSpit = Instantiate(spit, t.position, t.rotation);
    currentSpit.GetComponent<Rigidbody2D>().velocity = rb2d.velocity * spitSpeed;

  }
  public void Spit(int repetitions)
  {
    spitQueue += repetitions - 1;
    Spit();
  }
  public void ClearSpits()
  {
    spitQueue = 0;
  }

  public void Poop()
  {

  }
  public void Poop(int repetitions)
  {
    poopQueue += repetitions - 1;
    Poop();
  }
  public void ClearPoops()
  {
    poopQueue = 0;
  }

  public void Dash(float seconds)
  {
    dashTime = seconds;
    animator.SetTrigger("Dash");
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.CompareTag("Pipe") || collision.gameObject.CompareTag("Enemy"))
    {
      Die();
    }
  }

  public void Die()
  {
    Application.LoadLevel(Application.loadedLevel);
  }

  void OnTriggerEnter2D(Collider2D other)
  {

  }
}
