﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundRenderer : MonoBehaviour {
	public Transform ground;
	public List<Transform> trees;
	public List<Transform> bushes;
	public List<Transform> clouds;

	private GameObject player;
	private float lastSpawn;
	public float spacing;
	public float ahead;
	public float groundHeight;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player");
		lastSpawn = player.transform.position.x;
	}

	// Update is called once per frame
	void Update () {
		if (player.transform.position.x - lastSpawn > spacing) {
			SpawnAt(player.transform.position.x + ahead);
			lastSpawn = player.transform.position.x;
		}
	}

	void SpawnAt(float x) {
  		var tree = trees[Random.Range(0, trees.Count-1)];
		var bush = trees[Random.Range(0, trees.Count-1)];
		var cloud = trees[Random.Range(0, trees.Count-1)];

		Instantiate(ground, new Vector3(Mathf.RoundToInt(x), groundHeight, 0), Quaternion.identity);
	}
}
