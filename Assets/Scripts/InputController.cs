using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
  public Text text;
  public PlayerController player;
  private string buffer;
  private string[] keys;

  void Start()
  {
    keys = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
    buffer = "";
    ClearBuffer();
  }

  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape)) {
      ClearBuffer();
    }
    if (Input.GetKeyDown(KeyCode.Backspace)) {
      PopBuffer();
    }
    foreach (string key in keys)
    {
      if (Input.GetKeyDown(key))
      {
        FillBuffer(key);
        CheckCommand();
      }
    }
    DrawBuffer();
  }

  void DrawBuffer() {
    text.text = buffer;
  }

  void FillBuffer(string key)
  {
    buffer = buffer + key;
    if (buffer.Length > 8) {
      buffer = buffer.Substring(buffer.Length-8, 8);
    }
  }

  void CheckCommand()
  {
    {
      var re = new Regex(@"(fl(a+)(p+))$", RegexOptions.IgnoreCase);
      var matches = re.Matches(buffer);
      if (matches.Count > 0)
      {
        int repetitions = 0;
        bool redo = false;
        string substring = "";
        foreach (Match match in matches) {
          substring = match.Groups[1].Value;
          repetitions = match.Groups[2].Length;
          redo = match.Groups[3].Length > 1;
        }
        if (redo) {
          buffer = "flapp";
          player.ClearFlaps();
          player.Flap();
        } else {
          buffer = substring;
          player.Flap(repetitions);
        }
        return;
      }
    }
    {
      var re = new Regex(@"(d(a+)s(h+))$", RegexOptions.IgnoreCase);
      var matches = re.Matches(buffer);
      if (matches.Count > 0)
      {
        int repetitions = 0;
        bool redo = false;
        string substring = "";
        foreach (Match match in matches) {
          substring = match.Groups[1].Value;
          repetitions = match.Groups[2].Length;
          redo = match.Groups[3].Length > 1;
        }
        if (redo) {
          buffer = "dashh";
        } else {
          buffer = substring;
        }
        player.Dash(Mathf.Log(repetitions+1.7f) / 3.0f);
      }
    }
    {
      var re = new Regex(@"(d(i+)v(e+))$", RegexOptions.IgnoreCase);
      var matches = re.Matches(buffer);
      if (matches.Count > 0)
      {
        int repetitions = 0;
        bool redo = false;
        string substring = "";
        foreach (Match match in matches) {
          substring = match.Groups[1].Value;
          repetitions = match.Groups[2].Length;
          redo = match.Groups[3].Length > 1;
        }
        if (redo) {
          buffer = "divee";
        } else {
          buffer = substring;
        }
        player.Dive();
      }
    }
    {
      var re = new Regex(@"(sp(i+)(t+))$", RegexOptions.IgnoreCase);
      var matches = re.Matches(buffer);
      if (matches.Count > 0)
      {
        int repetitions = 0;
        bool redo = false;
        string substring = "";
        foreach (Match match in matches) {
          substring = match.Groups[1].Value;
          repetitions = match.Groups[2].Length;
          redo = match.Groups[3].Length > 1;
        }
        if (redo) {
          buffer = "spitt";
          player.ClearSpits();
          player.Spit();
        } else {
          buffer = substring;
          player.Spit(repetitions);
        }
      }
    }
    {
      var re = new Regex(@"(po(o+)(p+))$", RegexOptions.IgnoreCase);
      var matches = re.Matches(buffer);
      if (matches.Count > 0)
      {
        int repetitions = 0;
        bool redo = false;
        string substring = "";
        foreach (Match match in matches) {
          substring = match.Groups[1].Value;
          repetitions = match.Groups[2].Length;
          redo = match.Groups[3].Length > 1;
        }
        if (redo) {
          buffer = "poopp";
          player.ClearPoops();
          player.Poop();
        } else {
          buffer = substring;
          player.Poop(repetitions);
        }
      }
    }
  }

  void ClearBuffer()
  {
    buffer = "";
  }

  void PopBuffer() {
    buffer = buffer.Substring(0, buffer.Length-1);
  }
}
