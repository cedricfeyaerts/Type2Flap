﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PipeController : MonoBehaviour
{

  private Transform t;
  private bool scored;
  public float OffsetPosFirst;

  public float OffestPosLast;
  public float MaxHeight;
  public float MinHeight;
  public float presenceThreshold;
  public float topThreshold;
  private ScoreController score;
  private GameObject player;
    private Animator animator;
    private bool withPlant;
    private bool plantTriggered;


    // Use this for initialization
    void Start()
  {
    t = this.GetComponent<Transform>();
    scored = false;
    score = GameObject.Find("Score").GetComponent<ScoreController>();
    player = GameObject.Find("Player");
    animator = GetComponent<Animator>();
        withPlant = Random.Range(0.0f, 1.0f) < 0.1f;
        plantTriggered = false;
    // print(score);
    float y = Random.Range(MinHeight, MaxHeight);

    if (Random.Range(0.0f, 1.0f) > topThreshold)
    {
      y = -y;
      t.eulerAngles = new Vector3(0, 0, 180);
    }
    else
    {
      t.eulerAngles = new Vector3(0, 0, 0);
    }

    t.position = new Vector3(t.position.x, y);
  }

  // Update is called once per frame
  void Update()
  {
    if (t.position.x < player.transform.position.x && !scored)
    {
      scored = true;
      score.Increment();
    }else if (!plantTriggered && withPlant && (t.position.x < player.transform.position.x + 5))
        {
            animator.SetTrigger("PlantExit");
            plantTriggered = true;
        }
  }
}
