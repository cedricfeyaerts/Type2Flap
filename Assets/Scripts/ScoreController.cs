﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{

  private int score;
  private Text text;

  // Use this for initialization
  void Start()
  {
    text = GetComponent<Text>();
    score = 0;
  }

  // Update is called once per frame
  public void Increment()
  {
    score += 10;
    text.text = score.ToString();
  }
}
