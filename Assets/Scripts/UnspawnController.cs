﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnspawnController : MonoBehaviour {
  	private Transform t;

	// Use this for initialization
	void Start () {
		t = this.GetComponent<Transform>();
	}

	// Update is called once per frame
	void Update () {
		GameObject player = GameObject.Find("Player");
		if (player.transform.position.x - t.position.x > 30) {
			DestroyImmediate(gameObject);
		}
	}
}
